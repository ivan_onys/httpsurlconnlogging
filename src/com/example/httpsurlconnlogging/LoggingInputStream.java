package com.example.httpsurlconnlogging;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import android.util.Log;

public class LoggingInputStream extends InputStream {
	private static final String TAG = "LIS <<";
	// you may use this to distinguish output from different sockets
	private final Socket socket;
	private final InputStream in;
	private StringBuffer sb = new StringBuffer();

	private void logSb() {
		if (sb.length() == 0)
			return;
		
		int pos = sb.lastIndexOf("\n");
		
		if (-1 == pos) {
			// print all we have in sb
			pos = sb.length() - 1;
		}
		
		Log.i(TAG, sb.substring(0, pos+1));
		sb.delete(0, pos+1);
	}
	
	public LoggingInputStream(Socket socket, InputStream in)
			throws IOException {
		this.socket = socket;
		this.in = in;
	}

	public int read() throws IOException {
		int result = in.read();
		
		if (-1 == result  ||  (int)'\n' == result) {
			logSb();
		} else {
			sb.append((char)result);
		}
		
		return result;
	}

	public int read(byte[] b, int off, int len) throws IOException {
		int length = in.read(b, off, len);
		
		if (length != -1) {
			sb.append(new String(b, off, length));
		} 
		
		logSb();
		
		return length;
	}
}