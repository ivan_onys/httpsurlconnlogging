package com.example.httpsurlconnlogging;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	static {
		System.setProperty("jsse", "record,socket");
		Log.i("MainActivity", "jsse = " + System.getProperty("jsse", ""));
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button btnGet = (Button)findViewById(R.id.btn_get);
		btnGet.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MyHttpsClient.performGet("https://google.com");
			}
		});
	}
}
