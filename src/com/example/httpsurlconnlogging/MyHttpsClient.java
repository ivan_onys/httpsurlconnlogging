package com.example.httpsurlconnlogging;

import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

import android.os.AsyncTask;
import android.util.Log;

public class MyHttpsClient {
	private static final String TAG = "MyHttpsClient";
	
	static {
		try {
			Log.i(TAG, "setting our SSLSocketFactory");
			HttpsURLConnection.setDefaultSSLSocketFactory(
				new WireLogSSLSocketFactory(HttpsURLConnection.getDefaultSSLSocketFactory()));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static class GetTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... args) {
			HttpsURLConnection conn = null;
			
	        try {
	        	URL url = new URL(args[0]);
	            conn = (HttpsURLConnection)url.openConnection();
	            conn.setRequestMethod("GET");

	            conn.connect();
	            
	            int status = conn.getResponseCode();

	            Log.i(TAG, String.format("Response code: %d, message: %s", 
	            		status, conn.getResponseMessage()));
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        } finally {
	        	if (null != conn) {
	        		conn.disconnect();
	        	}
	        }

	        return "";
		}
	} // class LoginTask
	
    public static void performGet(String url) {
    	new GetTask().execute(url);
    } // login()
} // class MyHttpsClient
