package com.example.httpsurlconnlogging;

public class DelegationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public DelegationException(String message) {
		super(message);
	}

	public DelegationException(String message, Throwable cause) {
		super(message, cause);
	}

	public DelegationException(Throwable cause) {
		super(cause);
	}
}