package com.example.httpsurlconnlogging;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import android.util.Log;

public class LoggingOutputStream extends OutputStream {
	private static final String TAG = "LOS >>";
	private final OutputStream out;
	
	// you may use this to distinguish between ouputs from different sockets
	private final Socket socket; 

	private StringBuffer sb = new StringBuffer();

	private void logSb() {
		if (sb.length() == 0)
			return;
		
		int pos = sb.lastIndexOf("\n");
		
		if (-1 == pos) {
			// print all we have in sb
			pos = sb.length() - 1;
		}
		
		Log.i(TAG, sb.substring(0, pos+1));
		sb.delete(0, pos+1);
	}	
	
	public LoggingOutputStream(Socket socket, OutputStream out)
			throws IOException {
		this.out = out;
		this.socket = socket;
	}

	public void write(int b) throws IOException {
		out.write(b);
		
		if (b == (int)'\n') {
			logSb();
		} else {
			sb.append((char)b);
		}
	}

	public void write(byte[] b, int off, int length) throws IOException {
		out.write(b, off, length);
		
		sb.append(new String(b, off, length));
		logSb();
	}
}
