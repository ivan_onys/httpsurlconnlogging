package com.example.httpsurlconnlogging;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import android.util.Log;

public class WireLogSSLSocketFactory extends SSLSocketFactory {
	private static final String TAG = "WireLogSSLSocketFactory";
    private SSLSocketFactory factoryDelegate;

    public WireLogSSLSocketFactory(SSLSocketFactory sslSocketFactory) {
    	Log.i(TAG, "ctor()");
        this.factoryDelegate = sslSocketFactory;
    }

    public static class WireLogSocket extends SSLSocket {
    	private static final String TAG = "WireLogSocket";
        private final Delegator delegator;

        public WireLogSocket(SSLSocket delegate) 
        		throws IOException {
        	Log.i(TAG, "ctor()");
            this.delegator = new Delegator(this, SSLSocket.class,
//            		"org.apache.harmony.xnet.provider.jsse.SSLSocketWrapper");
            		delegate);
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
        	Log.i(TAG, "getOutputStream()");
        	OutputStream real = delegator.delegateTo("getOutputStream", new Class<?>[0]).invoke();
            return new LoggingOutputStream(null, real);
        }

        @Override
        public InputStream getInputStream() throws IOException {
        	Log.i(TAG, "getInputStream()");
        	InputStream real = delegator.delegateTo("getInputStream", new Class<?>[0]).invoke();
            return new LoggingInputStream(null, real);
        }

        @Override
		public void addHandshakeCompletedListener(
				HandshakeCompletedListener arg0) {
	       	Log.i(TAG, "addHandshakeCompletedListener()");
			delegator.invoke(arg0);
		}

		@Override
		public boolean getEnableSessionCreation() {
	       	Log.i(TAG, "getEnableSessionCreation()");
			return delegator.invoke();
		}

		@Override
		public String[] getEnabledCipherSuites() {
	       	Log.i(TAG, "getEnabledCipherSuites()");
			return delegator.invoke();
		}

		@Override
		public String[] getEnabledProtocols() {
	       	Log.i(TAG, "getEnabledProtocols()");
			return delegator.invoke();
		}

		@Override
		public boolean getNeedClientAuth() {
	       	Log.i(TAG, "getNeddClientAuth()");
			return delegator.invoke();
		}

		@Override
		public SSLSession getSession() {
	       	Log.i(TAG, "getSession()");
			return delegator.delegateTo("getSession", new Class<?>[0]).invoke();
		}

		@Override
		public String[] getSupportedCipherSuites() {
	       	Log.i(TAG, "getSupportedCipherSuites()");
			return delegator.invoke();
		}

		@Override
		public String[] getSupportedProtocols() {
	       	Log.i(TAG, "getSupportedProtocols()");
			return delegator.invoke();
		}

		@Override
		public boolean getUseClientMode() {
	       	Log.i(TAG, "getUseClientMode()");
			return delegator.invoke();
		}

		@Override
		public boolean getWantClientAuth() {
	       	Log.i(TAG, "getWantClientAuth()");
			return delegator.invoke();
		}

		@Override
		public void removeHandshakeCompletedListener(
				HandshakeCompletedListener arg0) {
	       	Log.i(TAG, "removeHandshakeCompletedListener()");
			delegator.invoke(arg0);
		}

		@Override
		public void setEnableSessionCreation(boolean arg0) {
	       	Log.i(TAG, "setEnableSessioncreation()");
			delegator.invoke(arg0);
		}

		@Override
		public void setEnabledCipherSuites(String[] arg0) {
	       	Log.i(TAG, "setEnabledCipherSuites()");
			delegator.invoke((Object)arg0);
		}

		@Override
		public void setEnabledProtocols(String[] arg0) {
	       	Log.i(TAG, "setEnabledProtocols()");
//			delegator.delegateTo("setEnabledProtocols", arg0.getClass()).invoke((Object)arg0);
			delegator.invoke((Object)arg0);
		}

		@Override
		public void setNeedClientAuth(boolean arg0) {
	       	Log.i(TAG, "setNeedClientAuth()");
			delegator.invoke(arg0);
		}

		@Override
		public void setUseClientMode(boolean arg0) {
	       	Log.i(TAG, "setUseClientMode()");
			delegator.invoke(arg0);
		}

		@Override
		public void setWantClientAuth(boolean arg0) {
	       	Log.i(TAG, "setWantClientAuth()");
			delegator.invoke(arg0);
		}

		@Override
		public void startHandshake() throws IOException {
	       	Log.i(TAG, "startHandshake()");
			delegator.invoke();
		}
    } // class WireLogSocket

	@Override
	public String[] getDefaultCipherSuites() {
		Log.i(TAG, "getDefaultCipherSuites()");
		return factoryDelegate.getDefaultCipherSuites();
	}

	@Override
	public String[] getSupportedCipherSuites() {
		Log.i(TAG, "getSupportedCipherSuites()");
		return factoryDelegate.getSupportedCipherSuites();
	}

	@Override
    public Socket createSocket(Socket s, String host, int port,
            boolean autoClose) throws IOException {
		Log.i(TAG, "createSocket1()");
/*		SSLParametersImpl spi = null; 
		
		try {
			Class<?> spiClass = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
			
			Method methodGetDefault = spiClass.getDeclaredMethod("getDefault");
			methodGetDefault.setAccessible(true);
			
			spi = (SSLParametersImpl) methodGetDefault.invoke(null, new Object[]{});
			
			Method methodClone = spiClass.getDeclaredMethod("clone");
			methodClone.setAccessible(true);
			
			spi = (SSLParametersImpl) methodClone.invoke(spi, new Object[]{});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
*/        return new WireLogSocket((SSLSocket)factoryDelegate.createSocket(s, host, port, autoClose));
    }

	@Override
	public Socket createSocket(InetAddress arg0, int arg1) throws IOException {
		Log.i(TAG, "createSocket2()");
//		return new WireLogSocket((SSLSocket)delegate.createSocket(arg0, arg1, arg2, arg3));
		return factoryDelegate.createSocket(arg0, arg1);
	}

	@Override
	public Socket createSocket(String arg0, int arg1, InetAddress arg2, int arg3)
			throws IOException, UnknownHostException {
		Log.i(TAG, "createSocket3()");
//		return new WireLogSocket((SSLSocket)delegate.createSocket(arg0, arg1, arg2, arg3));
		return factoryDelegate.createSocket(arg0, arg1, arg2, arg3);
	}

	@Override
	public Socket createSocket(InetAddress arg0, int arg1, InetAddress arg2,
			int arg3) throws IOException {
		Log.i(TAG, "createSocket4()");
//		return new WireLogSocket((SSLSocket)delegate.createSocket(arg0, arg1, arg2, arg3));
		return factoryDelegate.createSocket(arg0, arg1, arg2, arg3);
	}

	@Override
	public Socket createSocket(String host, int port) throws IOException,
			UnknownHostException {
		Log.i(TAG, "createSocket5()");
//		return new WireLogSocket((SSLSocket)delegate.createSocket(host, port));
		return factoryDelegate.createSocket(host, port);
	}
}
