Code is provided for logging of received and sent messages, in particular
HTTP headers, when using HttpsURLConnection class.

Usage notes:
 1. On button click performs request to https://google.com
 2. Logcat output will show messages with tags "LIS <<" and "LOS >>"
    for received and sent messages.
 3. Can be built and run both from eclipse and from command line.
 4. bnr.sh is one-click script to rebuild and run on emulator/device
    
Implementation details:
 1. Delegator class described in [2] was extended to provide method look-up in 
    superclasses.
    
Limitations:
 1. Doesn't distinguish output based on socket 
 
References:
 [1] http://www.javaspecialists.eu/archive/Issue169.html
 [2] http://www.javaspecialists.eu/archive/Issue168.html
 [3] http://stackoverflow.com/questions/15536252/how-to-enable-or-implement-wire-logging-for-httpsurlconnection-traffic-on-androi
 
 Contact:
  If you have suggestions / options how to do it easier or better, please
  mail to ivan.onyshchenko(at)gmail.com